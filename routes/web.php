<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::get('/categories', [\App\Http\Controllers\Admin\CategoriesController::class, 'index'])->name('categories');
Route::post('/categories-store', [\App\Http\Controllers\Admin\CategoriesController::class, 'store'])->name('categories.store');
Route::get('/categories/{id}/edit', [\App\Http\Controllers\Admin\CategoriesController::class, 'edit'])->name('categories.edit');
Route::post('/categories/{id}/update', [\App\Http\Controllers\Admin\CategoriesController::class, 'update'])->name('categories.update');
Route::delete('/categories/{id}/delete', [\App\Http\Controllers\Admin\CategoriesController::class, 'destroy'])->name('categories.delete');

Route::get('/products', [\App\Http\Controllers\Admin\ProductsController::class, 'index'])->name('manager.products');
Route::get('/products/create', [\App\Http\Controllers\Admin\ProductsController::class, 'create'])->name('create.product');
Route::post('/products/store', [\App\Http\Controllers\Admin\ProductsController::class, 'store'])->name('store.product');
Route::get('/products/{id}/edit', [\App\Http\Controllers\Admin\ProductsController::class, 'edit'])->name('edit.product');
