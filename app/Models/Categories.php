<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;
    protected $table='categories';
    protected $fillable = [
        'title',
    ];
    public function products()
    {
        return $this->hasMany(Products::class, 'id', 'categories_id');
    }
    public function storeData($request)
    {
        $data = [
            'title' => $request->title,
        ];
        return $this->create($data); // create là 1 hàm khi tạo dữ liệu
    }
    public function updateCaretory($request)
    {
        $item = $this->findOrFail($request->id);
        $data = [
            'title' => $request->title,
        ];
        return $item->update($data);
    }
}
