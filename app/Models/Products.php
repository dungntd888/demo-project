<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['title', 'description', 'content', 'price' , 'rating', 'categories_id', 'image'];
    public function category()
    {
        return $this->belongsTo(Categories::class, 'id');
    }

    public function getData()
    {
        return $this->with('category')->orderBy('id', 'desc')->get();
    }
    public function createProduct($request)
    {

        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content,
            'price' => $request->price,
            'rating' => $request->rate,
            'categories_id' => $request->category_id,
        ];
        if($request->hasFile('image')){
            $image = $request->file('image');
            $data['image'] ='image-job-' . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('uploads/job');
            $image->move($destinationPath, $data['image']);
        }
        return $this->create($data);

    }
}
