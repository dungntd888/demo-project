<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Products $products, Categories $categories)
    {
        $data = $products->getData();
        return view('products.listProducts', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Categories $categories)
    {
        $data = $categories->get();
        return view('products.createProduct', [
            'categories' => $data,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Products $products)
    {
        $request->validate([
            'title' => 'required|max:20',
            'description' => 'required|max:255',
            'content' => 'required|max:255',
            'rate' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'category_id' => 'required|max:50',
        ]);
        $create = $products->createProduct($request);
        return redirect()->route('manager.products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Products $products, Categories $categories)
    {
        $data = $products->findOrFail($id);
        // dd($data);
        $dataCate = $categories->get();
        return view('products.editProduct',[
            'productEdit' => $data,
            'categories' => $dataCate,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
