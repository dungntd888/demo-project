<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('AdminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item menu-open">
        <a href="#" class="nav-link {{ request()->is('home')? 'active' : '' }}">Dashboard</a>
        </li>
             <li class="nav-item menu-open">
          <a href="{{ route('categories') }}" class="nav-link {{ request()->is('categories')? 'active' : '' }}">Quản lý danh mục</a>
        </li>
        <li class="nav-item menu-open">
            <a href="{{ route('manager.products') }}" class="nav-link {{ request()->is('products')? 'active' : '' }}">Quản lý sản phẩm</a>
        </li>
        <li class="nav-item menu-open">
            <a href="#" class="nav-link {{ request()->is('home')? 'active' : '' }}">Cấu hình liên hệ</a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
