@extends('layouts.app')

@section('content')
    <style>
        .preview-img.preview-image img {
            width: 160px;
            height: 100px;
        }

        .preview-image {
            position: relative;
        }

        .remove-image {
            position: absolute;
            top: 0;
            left: 128px;
        }
        *{
            margin: 0;
            padding: 0;
        }
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }
        .rate:not(:checked) > input {
            position:absolute;
            top:-9999px;
        }
        .rate:not(:checked) > label {
            float:right;
            width:1em;
            overflow:hidden;
            white-space:nowrap;
            cursor:pointer;
            font-size:30px;
            color:#ccc;
        }
        .rate:not(:checked) > label:before {
            content: '★ ';
        }
        .rate > input:checked ~ label {
            color: #ffc700;
        }
        .rate:not(:checked) > label:hover,
        .rate:not(:checked) > label:hover ~ label {
            color: #deb217;
        }
        .rate > input:checked + label:hover,
        .rate > input:checked + label:hover ~ label,
        .rate > input:checked ~ label:hover,
        .rate > input:checked ~ label:hover ~ label,
        .rate > label:hover ~ input:checked ~ label {
            color: #c59b08;
        }

        /* Modified from: https://github.com/mukulkant/Star-rating-using-pure-css */

    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title text-center">Thêm mới sản phẩm</h3>
                                <form class="form-horizontal mt-4" method="POST" novalidate action=""
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tiêu đề<span class="help text-danger">*</span></label>
                                                <input type="text" name="title" class="form-control form-control-lg"
                                                    placeholder="Title" value="{{ $productEdit->title }}">
                                                @if ($errors->has('title'))
                                                    <span class="text-danger pt-2">{{ $errors->first('title') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Ảnh sản phẩm<span class="help text-danger"></span></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Tải lên</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" name="image" accept="image/*"
                                                        class="custom-file-input" id="image">
                                                    <label class="custom-file-label image" for="image">Chọn file</label>
                                                </div>
                                            </div>
                                            <span class="text-danger" id="error-image" style="display: none;"></span>
                                            @if ($errors->has('image'))
                                                <span
                                                    class="text-danger pt-2 error-image">{{ $errors->first('image') }}</span>
                                            @endif
                                            <div class="mt-3 mb-3 preview-img preview-image">
                                                <img id="ImgPreview" src="{{ asset('images/no_image.png') }}"
                                                    width="100%" />
                                                <button type="button" title="Remove image"
                                                    class="btn btn-danger remove-image" id="removeImage">x</button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Mô tả<span class="text-danger">*</span>
                                                </label>
                                                 <textarea name="description" row="4" class="form-control" placeholder="Mô tả sản phẩm"></textarea>
                                                 @if ($errors->has('description'))
                                                    <span class="text-danger pt-2">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Content<span class="help text-danger">*</span></label>
                                                <textarea name="content" row="4" class="form-control" placeholder="Content"></textarea>
                                                @if ($errors->has('Content'))
                                                    <span
                                                        class="text-danger pt-2">{{ $errors->first('Content') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Category<span class="help text-danger">*</span></label>
                                                <select name="category_id" class="form-control">
                                                     @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}" {{ ($productEdit->categories == $category->id) ? 'selected' : '' }}>{{ $category->title }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('Category-id'))
                                                    <span class="text-danger pt-2">{{ $errors->first('categories_id') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Giá<span class="help text-danger">*</span></label>
                                                <input type="text" name="price" class="form-control form-control-lg"
                                                    placeholder="Giá sản phẩm" value="{{ $productEdit->price }}">
                                                @if ($errors->has('price'))
                                                    <span class="text-danger pt-2">{{ $errors->first('price') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Rating<span class="help text-danger">*</span></label>
                                                <div class="rate">
                                                    <input type="radio" id="star5" name="rate" value=""  />
                                                    <label for="star5" title="text">5 stars</label>
                                                    <input type="radio" id="star4" name="rate" value="" />
                                                    <label for="star4" title="text">4 stars</label>
                                                    <input type="radio" id="star3" name="rate" value="" />
                                                    <label for="star3" title="text">3 stars</label>
                                                    <input type="radio" id="star2" name="rate" value="" />
                                                    <label for="star2" title="text">2 stars</label>
                                                    <input type="radio" id="star1" name="rate" value="" checked />
                                                    <label for="star1" title="text">1 star</label>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-center">
                                        <a href="" class="btn btn-secondary">Cancel</a>
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>
                                        Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Small boxes (Stat box) -->
            </div>

    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>

@endsection
@push('after-scripts')
<script>
    $(document).ready(function (){
        $("#removeImage").click(function(e) {
            e.preventDefault();
            $("#image").val("");
            $("#ImgPreview").attr("src", "");
            $('.preview-image').hide();
        });

        $('#image').bind('change', function () {
            var a = 1;
            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'svg']) == -1) {
                $('#error-image').html('Ảnh không đúng định dạng! Định dạng ảnh phải là JPG, JPEG, PNG hoặc GIF.');
                $('#error-image').slideDown("slow");
                $('.preview-image').hide();
                $('.error-image').hide();
                $('#image').val("");
                a = 0;
            } else {
                var picsize = (this.files[0].size);
                if (picsize > 10000000) {
                    $('#error-image').html('Ảnh không được lớn hơn 10MB.');
                    $('#error-image').slideDown("slow");
                    $('.preview-image').hide();
                    $('.error-image').hide();
                    $('#image').val("");
                    a = 0;
                } else {
                    a = 1;
                    $('#error-image').slideUp("slow");
                    $('.error-image').hide();
                }
                if (a == 1) {
                    $('#error-image').slideUp("slow");
                    var imgControlName = "#ImgPreview";
                    readURL(this, imgControlName);
                    $('.preview-image').show();
                }
            }
        });

    })
</script>
@endpush



